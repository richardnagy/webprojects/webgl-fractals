# WebGL Fractals

Rendering fractals on the web using flat shaders with Three.js

## Live Demo

https://richardnagy.gitlab.io/webprojects/webgl-fractals/

## Usage

1. Download repository

```bash
https://gitlab.com/richardnagy/webprojects/webgl-fractals
cd webgl-fractals
```

2. Install dependencies

```bash
yarn install
```

3. Build with webpack

```bash
yarn build
```